import { Paper, TextField, Button, Box, Typography } from "@mui/material";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

function Boards(props) {
    const { boards, handleAddBoard } = props;
    const [name, setName] = useState("");
    let navigate = useNavigate();

    function callBoardLists(id) {
        navigate(`boards/${id}`)
    }
    return (
        <>
            <Box
                sx={{
                    display: "flex", flexWrap: "wrap", columnGap: 12, rowGap: 4, pl: 17, pr: 17
                }}
            >

                {boards.map((board) => {
                    console.log(board)
                    return (

                        <Paper
                            sx={{
                                p: 2,
                                display: `flex`,
                                height: 70,
                                width: 280,
                                backgroundImage: `url(${board.prefs.backgroundImage})`,
                                backgroundSize: `cover`,
                                color: "white",
                                bgcolor: "darkblue",
                            }}
                            key={board.id}
                            onClick={() => callBoardLists(board.id)}
                        >
                            {board.name}
                        </Paper>

                    );
                })}
                {boards.length === 10 ? <Paper sx={{
                    p: 2,
                    display: `flex`,
                    height: 70,
                    width: 280,
                    color: "white",
                    bgcolor: "black"
                }}>Maximum Board limit Reached</Paper> : <>
                    <TextField
                        placeholder="New Board"
                        color="primary"
                        focused
                        value={name}
                        onChange={(event) => {
                            setName(event.target.value);
                        }}
                        sx={{ color: 'white' }}
                    />
                    <Button
                        sx={{ height: 50, width: 180 }}
                        variant="contained"
                        onClick={(event) => {
                            if (name) {
                                handleAddBoard(name);
                                setName('');
                            }
                        }}
                    >
                        Add New Board
                    </Button>
                </>
                }

            </Box>
        </>
    )
}

export default Boards;