import React, { useState, useEffect } from "react";
import { getAllCards, addCard, deleteCard } from "../../Authorization/KeyToken";
import { Box, TextField, IconButton } from "@mui/material";
import CheckList from "../CheckLists/CheckListContainer";
import { useNavigate } from "react-router-dom";


function CardContainer({ listId }) {
    const [cards, setCards] = useState([]);
    const [name, setName] = useState("");
    let navigate = useNavigate();
    useEffect(() => {
        getAllCards(listId).then((response) => {
            setCards(response);
        }).catch((error) => {
            navigate('/error');
        })
    }, [listId]);

    function handleAddCard() {
        addCard(name, listId)
            .then((response) => {
                console.log(response);
                setCards((prevCards) => [...prevCards, response]);
                setName("");
            }).catch((error) => {
                navigate('/error');
            })
    }

    function handleCardDelete(cardId) {
        console.log("delete card");
        deleteCard(cardId).then((response) => {
            let updatedCardArray = cards.filter((card) => {
                return card.id !== cardId;
            })
            setCards(updatedCardArray);
        }).catch((error) => {
            navigate('/error');
        })
    }

    return (
        <Box>
            <Box style={{ display: "flex", flexDirection: "column", flexWrap: "wrap", gap: "10px" }}>
                {cards.map((card, index) => (
                    <Box
                        key={index}
                        sx={{
                            display: "flex",
                            justifyContent: "space-between",
                            backgroundColor: "white",
                            borderRadius: "5px",
                            padding: "10px",
                            cursor: "pointer",
                            transition: "0.3s",
                            "&:hover": {
                                backgroundColor: "#f0f0f0",
                            },
                        }}
                    >
                        <Box>
                            <CheckList card={card} />
                        </Box>

                        <i class="fa-solid fa-delete-left" onClick={() => {
                            handleCardDelete(card.id)
                        }}></i>
                    </Box>

                ))}
            </Box>

            <Box sx={{ display: "flex", alignItems: "center", marginTop: "10px" }}>
                <TextField
                    placeholder="Add A Card"
                    color="primary"
                    focused
                    value={name}
                    onChange={(event) => {
                        setName(event.target.value);
                    }}
                    sx={{ color: 'white' }}
                />
                <IconButton
                    color="primary"
                    onClick={() => {
                        if (name) {
                            handleAddCard(name);
                        }
                    }}
                    sx={{ backgroundColor: "blue", color: "white", marginLeft: "10px", fontSize: 20 }}
                ><i class="fa-solid fa-plus"></i>
                </IconButton>

            </Box>
        </Box>
    );
}

export default CardContainer;