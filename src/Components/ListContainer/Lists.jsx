import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { getLists, addList, deleteList } from "../../Authorization/KeyToken";
import { Box, Typography, TextField, Button } from "@mui/material";
import CardContainer from "../CardContainer/CardContainer";
import { useNavigate } from "react-router-dom";

function Lists() {
    const [lists, setLists] = useState([]);
    const [newList, setNewList] = useState("");
    let params = useParams();
    let navigate = useNavigate()
    useEffect(() => {
        getLists(params.id)
            .then((response) => {
                setLists(response);
            }).catch((error) => {
                navigate('/error');
            })
    }, [])

    function handleAddList(name) {
        addList(params.id, name).then((response) => {
            return setLists([...lists, response]);
        }).catch((error) => {
            navigate('/error');
        })
    }

    function handleDeleteList(id) {
        deleteList(id).then((response) => {
            const filteredList = lists.filter((list) => {
                return list.id !== id;
            })
            setLists(filteredList);
        }).catch((error) => {
            navigate('/error');
        })
    }
    return (
        <Box sx={{ display: "flex", flexWrap: "wrap" }}>
            {lists.map((list) => {
                return <Box
                    sx={{
                        display: "inline-block",
                        width: 300,
                        minHeight: 50,
                        bgcolor: "#F1F2F4",
                        position: "relative",
                        p: 2,
                        borderRadius: 4,
                        marginLeft: 3,
                        marginBottom: 3
                    }}
                    key={list.id}
                >
                    <Box
                        sx={{ display: "flex", flexDirection: "column", rowGap: 2 }}
                    >
                        <Box sx={{ display: "flex", justifyContent: "space-between" }}>
                            <Typography variant="h6">{list.name}</Typography>
                            <i class="fa-solid fa-trash" onClick={() => handleDeleteList(list.id)}></i>
                        </Box>
                        <CardContainer listId={list.id} />
                    </Box>
                </Box>


            })}
            <Box sx={{
                marginTop: 2,
                marginLeft: 3,
                display: "flex",
                columnGap: 1
            }}>
                <TextField
                    placeholder="Add New List"
                    color="primary"
                    focused
                    value={newList}
                    onChange={(event) => {
                        setNewList(event.target.value);
                    }}
                    sx={{ color: 'white' }}
                />
                <Button
                    sx={{ height: 50, width: 180 }}
                    variant="contained"
                    onClick={(event) => {
                        if (newList) {
                            handleAddList(newList);
                            setNewList('');
                        }
                    }}
                >
                    Add New List
                </Button>
            </Box>
        </Box>
    )
}

export default Lists;