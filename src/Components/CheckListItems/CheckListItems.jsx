import React, { useEffect, useState } from "react";
import Button from "@mui/material/Button";
import { getCheckItems, addCheckItems, deleteCheckItems, updateItemState } from "../../Authorization/KeyToken";
import { Box, Checkbox, FormControlLabel, FormGroup, TextField } from "@mui/material";
import { useNavigate } from "react-router-dom";
import ProgressBar from "./ProgressBar";

function CheckListItems({ checklist }) {
    const [checkItems, setCheckItems] = useState([]);
    const [name, setName] = useState("");
    const [progress, setProgress] = useState(0);
    let navigate = useNavigate();
    console.log(checklist);
    useEffect(() => {
        getCheckItems(checklist.id)
            .then((checkItems) => {
                console.log(checkItems);
                setCheckItems(checkItems);
            }).catch((error) => {
                navigate('/error');
            })
    }, [checklist.id]);

    const handleAddCheckItem = (name) => {
        addCheckItems(name, checklist.id)
            .then((response) => {
                setCheckItems([...checkItems, response]);
                console.log(response);
            }).catch((error) => {
                navigate('/error');
            })
    };

    const handleDeleteCheckItem = (checkListId, checkItemId) => {
        deleteCheckItems(checkListId, checkItemId).then((response) => {
            let filteredCheckedItems = checkItems.filter((checkItem) => {
                return checkItem.id !== checkItemId;
            });
            setCheckItems(filteredCheckedItems);
        }).catch((error) => {
            navigate('/error');
        })
    }

    const handleUpdateState = (checkItemId, state) => {
        const newState = state === "incomplete" ? "complete" : "incomplete";
        updateItemState(checklist.idCard, checkItemId, newState).then((response) => {
            console.log("checkitem", response);
            let updatedCheckItems = checkItems.map((item) => {
                return item.id === checkItemId ? { ...item, state: newState } : item;
            });
            setCheckItems(updatedCheckItems);
        }).catch((error) => {
            navigate('/error');
        });
    }

    return (
        <Box>
            <Box sx={{ display: 'flex', alignItems: 'center' }}>
                <ProgressBar checkItems={checkItems} />
            </Box>
            <FormGroup>
                {checkItems.map((checkitem) => {
                    console.log("checkitem", checkitem);
                    return (
                        <Box key={checkitem.id} style={{ display: "flex" }}>
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        checked={checkitem.state === "complete"}
                                        onChange={() =>
                                            handleUpdateState(checkitem.id, checkitem.state)
                                        }
                                    />
                                }
                                label={checkitem.name}
                            />
                            <i style={{ marginTop: 13 }} class="fa-solid fa-delete-left" onClick={() => handleDeleteCheckItem(checklist.id, checkitem.id)}></i>
                        </Box>
                    );
                })}
            </FormGroup>
            <br />
            <TextField
                size="small"
                placeholder="Item"
                color="primary"
                focused
                value={name}
                onChange={(event) => {
                    setName(event.target.value);
                }}
                variant="outlined"
                sx={{ color: "white" }}
            />
            <Button
                variant="outline-secondary"
                id="button-addon2"
                onClick={(event) => {
                    if (name) {
                        handleAddCheckItem(name);
                        setName("");
                    }
                }}
            >
                Add
            </Button>
        </Box>
    );

}

export default CheckListItems;

