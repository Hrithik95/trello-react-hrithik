import React from "react";
import { LinearProgress } from "@mui/material";
function ProgressBar({ checkItems }) {
    let checkedItemsList = checkItems.filter((item) => {
        return item.state === 'complete';
    });
    return (
        <>
            <LinearProgress variant="determinate" value={checkItems.length === 0 ? 0 : (checkedItemsList.length / checkItems.length) * 100} style={{ width: '35vw' }} />
        </>
    )
}




export default ProgressBar;