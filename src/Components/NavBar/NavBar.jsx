import React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import { Link } from "react-router-dom";
import { Button } from "@mui/material";
import './NavBar.css'

export default function NavBar() {
    return (
        <Box>
            <AppBar position="static" style={{ width: '100vw', margin: 0, padding: 0 }}>
                <Toolbar variant="dense" style={{ margin: 0, padding: 0 }}>
                    <IconButton
                        edge="start"
                        color="inherit"
                        aria-label="menu"
                        sx={{ mr: 2 }}
                    >
                    </IconButton>

                    <Box sx={{ height: 30, display: "flex", columnGap: '32vw', justifyContent: "center", alignItems: "center" }}>
                        <Link to={'/'}>
                            <Button sx={{ color: "white", fontSize: 30 }}>Boards</Button>
                        </Link>
                        <img
                            src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7a/Trello-logo-blue.svg/2560px-Trello-logo-blue.svg.png"
                            style={{ height: "100%" }}
                        />
                    </Box>

                </Toolbar>
            </AppBar>
        </Box>
    );
}
