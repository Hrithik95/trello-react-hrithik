import { Paper, TextField, Button, Box } from "@mui/material";
import React, { useState } from "react";
import Boards from "../Boards/Boards";
import Lists from "../ListContainer/Lists";
import { Routes, Route } from "react-router-dom";
import ErrorPage from "../ErrorPage/ErrorPage";

function HomePage(props) {
    const { boards, handleAddBoard } = props;
    return (
        <>
            <Routes>
                <Route path='/' element={<Boards boards={boards} handleAddBoard={handleAddBoard} />} />
                <Route path='boards/:id' element={<Lists />} />
                <Route path="/error" element={<ErrorPage />}></Route>
            </Routes>

        </>
    )
}

export default HomePage;