import { Typography } from "@mui/material";
import React from "react";


function ErrorPage() {
    return <>
        <Typography variant="h2">Something went wrong</Typography>
    </>
}

export default ErrorPage;