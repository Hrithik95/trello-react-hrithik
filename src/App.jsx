import { useState, useEffect } from 'react';
import HomePage from './Components/HomePage/HomePage';
import { getAllBoards, addBoard } from './Authorization/KeyToken';
import './App.css'
import NavBar from './Components/NavBar/NavBar';
import { Box } from '@mui/material';
import { useNavigate } from 'react-router-dom';

function App() {
  const [boards, setBoards] = useState([]);
  let navigate = useNavigate();
  useEffect(() => {
    getAllBoards()
      .then((boards) => {
        setBoards(boards);
      })
      .catch((err) => {
        navigate('/error');
      })
  }, []);

  const handleAddBoard = (name) => {
    addBoard(name)
      .then((response) => {
        setBoards([...boards, response]);
      })
      .catch((err) => {
        navigate('/error');
      });
  };

  return (
    <Box sx={{ display: "flex", flexDirection: "column", rowGap: 10 }}>
      <NavBar />
      <HomePage boards={boards} handleAddBoard={handleAddBoard} />
    </Box>
  )
}

export default App
