import axios from "axios";

const apiKey = "ed1397b2e45f8ff8373d2597779adf84";
const token = "ATTA48224000b09d6e0e830a9614270c188478aaefd1f93f28b85c503e01edc06e3d919922B6";


export const getAllBoards = () => {
    return axios.get(`https://api.trello.com/1/members/me/boards?key=${apiKey}&token=${token}`)
        .then((response) => {
            // console.log(response.data);
            return response.data;
        })
}

export const addBoard = (name) => {
    const addBoardUrl = `https://api.trello.com/1/boards/?name=${name}&key=${apiKey}&token=${token}`;
    return axios.post(addBoardUrl).then((response) => {
        // console.log(response)
        return response.data;
    });

}

export const getLists = (id) => {
    return axios.get(`https://api.trello.com/1/boards/${id}/lists?key=${apiKey}&token=${token}`).then((response) => {
        // console.log(response.data);
        return response.data;
    })
}

export const addList = (id, name) => {
    return axios.post(`https://api.trello.com/1/boards/${id}/lists?name=${name}&key=${apiKey}&token=${token}`).then((response) => {
        return response.data;
    })
}

export const deleteList = (id) => {
    return axios.put(`https://api.trello.com/1/lists/${id}?closed=true&key=${apiKey}&token=${token}`).then((response) => {
        return response.data;
    })
}

export const getAllCards = (listID) => {
    return axios.get(`https://api.trello.com/1/lists/${listID}/cards?key=${apiKey}&token=${token}`).then((response) => {
        return response.data;
    })
}

export const addCard = (name, listId) => {
    return axios.post(`https://api.trello.com/1/cards?name=${name}&idList=${listId}&key=${apiKey}&token=${token}`).then((response) => {
        return response.data;
    })
}

export const deleteCard = (id) => {
    return axios.delete(`https://api.trello.com/1/cards/${id}?key=${apiKey}&token=${token}`).then((response) => {
        return response.data;
    });
};

export const getCheckLists = (cardId) => {

    const checkListUrl = `https://api.trello.com/1/cards/${cardId}/checklists?key=${apiKey}&token=${token}`;
    return axios.get(checkListUrl).then((response) => response.data)
}

export const addCheckList = (name, cardId) => {
    return axios.post(`https://api.trello.com/1/checklists?name=${name}&idCard=${cardId}&key=${apiKey}&token=${token}`).then((response) => response.data);
}

export const deleteCheckList = (cardId, checkListId) => {
    return axios.delete(`https://api.trello.com/1/cards/${cardId}/checklists/${checkListId}?key=${apiKey}&token=${token}`).then((response) => response.data);
}

export const getCheckItems = (checkListId) => {
    return axios.get(`https://api.trello.com/1/checklists/${checkListId}/checkItems?key=${apiKey}&token=${token}`).then((response) => {
        return response.data;
    })
}

export const addCheckItems = (name, checkListId) => {
    return axios.post(`https://api.trello.com/1/checklists/${checkListId}/checkItems?name=${name}&key=${apiKey}&token=${token}`).then((response) => {
        return response.data;
    })
}

export const deleteCheckItems = (checkListId, checkItemId) => {
    return axios.delete(`https://api.trello.com/1/checklists/${checkListId}/checkItems/${checkItemId}?key=${apiKey}&token=${token}`)
}

export const updateItemState = (cardId, idCheckItem, status) => {
    const data = {
        state: status
    };

    return axios.put(`https://api.trello.com/1/cards/${cardId}/checkItem/${idCheckItem}?key=${apiKey}&token=${token}`, data).then((response) => {
        return response.data;
    })
}

